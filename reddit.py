def fib_seq(n):
    a = 0
    b = 1
    if not n:
        return a
    elif n == 1:
        return b
    else:
        for i in range(2,n):
            c = a + b
            a = b
            b = c
        return b


print(fib_seq(5))